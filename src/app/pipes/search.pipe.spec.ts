import { SearchPipe } from './search.pipe';
describe('SearchPipe', () => {
  let pipe: SearchPipe;
  const items = [{
    flightNumber: 1193,
    flightScheduleDate: '2019-07-13',
    id: '20190713+KL+1193',
    haul: 'MEDIUM',
    route: ['AMS', 'BGO'],
    airline: {code: 'KL', name: 'KLM ROYAL DUTCH AIRLINES'},
    flightStatusPublic: 'DEPARTED',
    flightStatusPublicLangTransl: 'Departed'
  }];
  beforeEach(() => {
    pipe = new SearchPipe();
  });
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('should return empty array if there is no items passed', () => {
    expect(pipe.transform([], '1')).toEqual([]);
  });
  it('should return  array if there is no search text passed', () => {
    expect(pipe.transform(items, '')).toEqual(items);
  });
  it('should return matching array object if search text matches with flightNumber', () => {
    expect(pipe.transform(items, '1')).toEqual(items);
  });
  it('should return empty if search text does not match with any flightNumber', () => {
    expect(pipe.transform(items, '2')).toEqual([]);
  });
});


