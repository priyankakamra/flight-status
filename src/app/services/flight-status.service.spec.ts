import {async, inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {FlightStatusService} from './flight-status.service';


describe('FlightStatusService', () => {
  let flightStatusSvc: FlightStatusService;
  let httpMock: HttpTestingController;
  let originalTimeout;
  beforeEach(() => {
    TestBed.configureTestingModule({

      providers: [FlightStatusService],
      imports: [HttpClientTestingModule]
    });
    flightStatusSvc = TestBed.get(FlightStatusService);
    httpMock = TestBed.get(HttpTestingController);
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
  });
  it('should be created', inject([FlightStatusService], (service: FlightStatusService) => {
    expect(flightStatusSvc).toBeTruthy();
  }));

  it('should execute get flight status service', async(() => {
    spyOn(flightStatusSvc, 'getFlightStatus').and.callThrough();
    flightStatusSvc.getFlightStatus();
    expect(flightStatusSvc.getFlightStatus).toHaveBeenCalled();
  }));
});



