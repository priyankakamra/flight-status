import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, finalize, map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FlightStatusService {

  constructor(private readonly http: HttpClient) {
  }

  /**
   * API call to get flight status
   */
  getFlightStatus(): Observable<any> {
    const startRange = new Date().toISOString().split('.')[0] + 'Z';
    const endRange = new Date(new Date().getTime() + 60 * 60 * 1 * 1000).toISOString().split('.')[0] + 'Z';
    const endpoint = `https://api.airfranceklm.com/opendata/flightstatus?startRange=` + startRange +
      `&endRange=` + endRange + `&origin=AMS`;
    const headers = new HttpHeaders().set('api-key', 'rtxx9g4e4smfxyw8uzhgy8as').set('Accept', 'application/hal+json;charset=UTF-8');
    return this.http.get(endpoint, {headers, observe: 'response'}).pipe(
      map((response) => response),
      catchError(err => {
        console.log('Problem getting data from api', err);
        return of([]);
      }),
      finalize(() => console.log('finalize error'))
    );
  }


}
