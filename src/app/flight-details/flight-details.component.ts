import {Component, OnInit, OnDestroy} from '@angular/core';
import {FlightStatusService} from '../services/flight-status.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-flight-details',
  templateUrl: './flight-details.component.html',
  styleUrls: ['./flight-details.component.scss']
})
export class FlightDetailsComponent implements OnInit, OnDestroy {
  public data;
  public searchText: string;
  private sub: Subscription;

  constructor(private flightStatusSvc: FlightStatusService) {
  }

  ngOnInit() {
    this.getFlightData();
  }

  private getFlightData() {
    this.sub = this.flightStatusSvc.getFlightStatus().subscribe(res => {
      this.data = res.body.operationalFlights;
    });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

}
